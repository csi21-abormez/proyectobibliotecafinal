import { Component, OnInit } from '@angular/core';
import {LibroService} from "../../../Servicios/libro.service";
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {Router} from "@angular/router";

@Component({
  selector: 'app-formulario',
  templateUrl: './formulario.component.html',
  styleUrls: ['./formulario.component.css']
})
export class FormularioComponent implements OnInit {
public libro: any;
  public form: any;
  public currentStatus = 1;
  public documentId: any;
  public nuevoLibroFormulario = new FormGroup({
    edicion: new FormControl('' ),
    autor: new FormControl('', Validators.required),
    editorial:new FormControl(''),
    lanzamiento: new FormControl(''),
    portada: new FormControl(''),
    titulo: new FormControl('',Validators.required ),
    id: new FormControl(''),
    prestado: new FormControl(false)
  });

  constructor(private libroService: LibroService, private router:Router) {
    this.nuevoLibroFormulario.setValue({
      edicion: '',
      autor: '',
      editorial: '',
      lanzamiento : '',
      titulo: '',
      id: '',
      portada: '',
      prestado:false

    })
  }

  ngOnInit(): void {
  }

  public nuevoLibro( form: any, documentId = this.documentId,){
    console.log('status. $(this.currentStatus}');
    if(this.currentStatus == 1){
      let data = {
        titulo : form.titulo,
        autor: form.autor,
        edicion: form.edicion,
        editorial: form.editorial,
        lanzamiento: form.lanzamiento,
        portada: form.portada,
        prestado: false,
      }
      this.libroService.crearLibro(data).then(() => {
        console.log('Documento creado correctamente!');
        this.nuevoLibroFormulario.setValue({
          titulo: '',
          autor: '',
          edicion: '',
          editorial: '',
          lanzamiento: '',
          id: '',
          portada: '',
          prestado: false,
        });
        this.router.navigateByUrl('Libro')
      }, (error) => {
        console.error(error);
      });
    }else {
      let data = {
        titulo : form.titulo,
        autor: form.autor,
        edicion: form.edicion,
        editorial: form.editorial,
        lanzamiento: form.lanzamiento,
        portada: form.portada,
          prestado: false,
      };
      this.documentId = form.id;
      this.libroService.actualizarLibro(this.documentId, data).then(() => {
        this.currentStatus = 1;
        this.nuevoLibroFormulario.setValue({
          titulo: '',
          autor: '',
          edicion: '',
          editorial: '',
          lanzamiento: '',
          id: '',
          portada: '',
          prestado: false,
        });
        console.log('Documento editado correctamente!');
        this.router.navigateByUrl('Libro')
      }, (error) => {
        console.log(error);
      });
    }
  }





}
