import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {LibroComponent} from "./libro.component";
import {FormularioComponent} from "./formulario/formulario.component";
import {EditarComponent} from "./editar/editar.component";




const routes: Routes = [

  {path: '', component:LibroComponent },
  {path: 'formulario', component: FormularioComponent},
  {path: 'editar/:id', component: EditarComponent}




];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class LibroRoutingModule { }
