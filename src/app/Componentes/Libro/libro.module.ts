import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { LibroRoutingModule } from './libro-routing.module';
import {LibroComponent} from "./libro.component";
import {ReactiveFormsModule} from "@angular/forms";


import { FormularioComponent } from './formulario/formulario.component';
import { EditarComponent } from './editar/editar.component';



@NgModule({
  declarations: [LibroComponent, FormularioComponent, EditarComponent],
  imports: [
    CommonModule,
    LibroRoutingModule,
    ReactiveFormsModule
  ]
})
export class LibroModule { }
