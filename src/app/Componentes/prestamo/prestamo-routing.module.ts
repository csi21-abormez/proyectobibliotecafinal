import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {PrestamoComponent} from "./prestamo.component";
import {FormularioPrestamoComponent} from "./formulario-prestamo/formulario-prestamo.component";
import {EditarprestamoComponent} from "./InformacionPrestamo/editarprestamo.component";

const routes: Routes = [
  {path:'', component: PrestamoComponent},
  {path:'prestamoFormulario', component: FormularioPrestamoComponent},
  {path: 'editarPrestamo/:id', component: EditarprestamoComponent}

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PrestamoRoutingModule { }
