import { Component, OnInit } from '@angular/core';
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {PrestamoService} from "../../../Servicios/prestamo.service";
import {LibroService} from "../../../Servicios/libro.service";
import {SocioService} from "../../../Servicios/socio.service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-formulario-prestamo',
  templateUrl: './formulario-prestamo.component.html',
  styleUrls: ['./formulario-prestamo.component.css']
})
export class FormularioPrestamoComponent implements OnInit {
  public socios:any[] = [];
  public libros: any[] = [];
  public prestamo:any  = {};
  public libroauxuliar:any = {};
  public form:any;
  public documentId:any;
  public nuevoPrestamoFormulario = new FormGroup({
    id: new FormControl(''),
    libro: new FormControl('',Validators.required ),
    socio: new FormControl('',Validators.required ),
    fechaPrestamo: new FormControl('' ),
    fechaDevolucion: new FormControl(''),
  });

  constructor(private prestamoService: PrestamoService, private libroService: LibroService, private socioService: SocioService, private router:Router) {

  }

  ngOnInit(): void {
    this.prestamo = [];
    this.libroService.obtenerLibrosSinPrestar().subscribe((librosSnapshot: any) => {
      this.libros = [];
      librosSnapshot.forEach((libroData: any) =>{
        this.libros.push({
          id: libroData.payload.doc.id,
          data: libroData.payload.doc.data(),
        });
      });
    });
    console.log(this.libros)
    this.socioService.obtenerSocios().subscribe((socioSnapchot: any) => {
      this.socios = [];
      socioSnapchot.forEach((socioData: any) => {
        this.socios.push({
          id: socioData.payload.doc.id,
          data: socioData.payload.doc.data(),
        });
      });
    });

  }


  public nuevoPrestamo(form:any, documentId = this.documentId){
    let libro = this.libros.find(l => l.id == form.libro)
    let socio = this.socios.find(s => s.id == form.socio)
    let data = {
      libro:form.libro,
      socio:form.socio,
      fechaDevolucion:null,
      fechaPrestamo: new Date(),
      nombrelibro: libro.data.titulo,
      nombresoscio: socio.data.nombre,

    }
    console.log(libro)
   this.prestamoService.crearPrestamo(data).then(() =>{
      console.log('Documento creado correctamente');
     let createSuscribe =   this.libroService.obtenerLibro(form.libro).subscribe((libro) => {
        this.libroauxuliar = {
          id: form.libro,
          data: libro.payload.data(),
        };
        console.log(this.libroauxuliar)
        this.libroauxuliar.data.prestado = true;
        this.libroService.actualizarLibro(this.libroauxuliar.id, this.libroauxuliar.data)
        console.log(this.libroauxuliar)
       createSuscribe.unsubscribe();
      });
     this.router.navigateByUrl('Prestamo')
    });



  }



}

