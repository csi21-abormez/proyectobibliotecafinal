import { Component, OnInit } from '@angular/core';
import {PrestamoService} from "../../Servicios/prestamo.service";
import {LibroService} from "../../Servicios/libro.service";
import {SocioService} from "../../Servicios/socio.service";
import {Router} from "@angular/router";
import {Location} from "@angular/common";
import {timeout} from "rxjs/operators";

@Component({
  selector: 'app-prestamo',
  templateUrl: './prestamo.component.html',
  styleUrls: ['./prestamo.component.css']
})
export class PrestamoComponent implements OnInit {
  listaPrestamos: any[] = [];
  libro: any[]=[];
  socio: any[]= [];
  prestamo:any ={};
  libroauxiliar: any= {};



  constructor(private prestamoService: PrestamoService, private libroService: LibroService, private socioService: SocioService, private router: Router, private location: Location) {

    this.prestamoService.obtenerPrestamoNoDevuelto().subscribe(prestamo =>{
      this.listaPrestamos = [];
      prestamo.forEach( prestamo => {
        this.listaPrestamos.push({
        id: prestamo.payload.doc.id,
          data: prestamo.payload.doc.data(),
        });
      });
    });
    console.log(this.prestamo)
  }

  ngOnInit(): void {
    this.listaPrestamos = [];
  }

  public eliminarPrestamo(prestamo: any){
    let eliminarSuscribe = this.libroService.obtenerLibro(prestamo.data.libro).subscribe(libro => {
      this.libroauxiliar = {
        id: prestamo.data.libro,
        data: libro.payload.data()
      }

      console.log(this.libroauxiliar);
      this.libroauxiliar.data.prestado = false
      prestamo.data.fechaDevolucion = new Date();
      this.libroService.actualizarLibro(this.libroauxiliar.id, this.libroauxiliar.data).then(() => {})
      this.prestamoService.actualizarPrestamo(prestamo.id, prestamo.data).then(() => {})
      alert('Prestamo devuelto correctamente');
      eliminarSuscribe.unsubscribe()
      this.router.navigateByUrl('Prestamo')

      console.log(this.libroauxiliar)



    }) ;

  }




  }
