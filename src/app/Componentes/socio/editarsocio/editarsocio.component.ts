import { Component, OnInit } from '@angular/core';
import {FormControl, FormGroup} from "@angular/forms";
import {SocioService} from "../../../Servicios/socio.service";
import {ActivatedRoute, Router} from "@angular/router";

@Component({
  selector: 'app-editarsocio',
  templateUrl: './editarsocio.component.html',
  styleUrls: ['./editarsocio.component.css']
})
export class EditarsocioComponent implements OnInit {

  socio?:any;
  id: any;
  public nuevoSocioFormulario = new FormGroup({
    id: new FormControl(''),
    nombre: new FormControl(''),
    apellido: new FormControl(''),
    dni: new FormControl(''),
    fechaNacimiento: new FormControl(''),
    foto: new FormControl('')

  })

  constructor(private socioService: SocioService, private activatedRoute: ActivatedRoute, private router:Router) { }

  ngOnInit(): void {
    this.activatedRoute.paramMap.subscribe((params) => {
      this.id = <string>params.get('id');

      this.editarSocio(this.id);
    });
  }

  public editarSocio(form:any, id = this.id){
    let editSuscribe = this.socioService.obtenerSocio(id).subscribe((data:any)=>{
      this.nuevoSocioFormulario.setValue({
        id: this.id,
        nombre: data.payload.data()['nombre'],
        apellido: data.payload.data()['apellido'],
        dni: data.payload.data()['dni'],
        fechaNacimiento: data.payload.data()['fechaNacimiento'],
        foto: data.payload.data()['foto'],
      });
      data = {
        nombre : form.nombre,
        apellido: form.apellido,
        dni: form.dni,
        fechaNacimiento: form.fechaNacimiento,
        foto: form.foto
      }
      this.socioService.actualizarSocio(id, data).then(()=>{
        alert('Documento editado correctamente');
      }, (error) => {
        console.log(error);
      });
        editSuscribe.unsubscribe();
        this.router.navigateByUrl('Socio')
    });
  }

  irAtras(){
    this.router.navigateByUrl('Socio')
  }
}
