import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {AngularFireModule} from "@angular/fire/compat";
import {environment} from "../environments/environment";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {LibroModule} from "./Componentes/Libro/libro.module";
import { NavbarComponent } from './Componentes/navbar/navbar.component';
import {SocioModule} from "./Componentes/socio/socio.module";
import {PrestamoModule} from "./Componentes/prestamo/prestamo.module";

import {AngularFireAuthModule} from "@angular/fire/compat/auth";
import {AngularFirestoreModule} from "@angular/fire/compat/firestore";
import {DashboardComponent} from "./Componentes/login/dashboard/dashboard.component";
import {SignInComponent} from "./Componentes/login/sign-in/sign-in.component";
import {SignUpComponent} from "./Componentes/login/sign-up/sign-up.component";
import {ForgotPasswordComponent} from "./Componentes/login/forgot-password/forgot-password.component";
import {VerifyEmailComponent} from "./Componentes/login/verify-email/verify-email.component";





@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    DashboardComponent,
    SignInComponent,
    SignUpComponent,
    ForgotPasswordComponent,
    VerifyEmailComponent



  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    AngularFireModule.initializeApp(environment.firebase),
    FormsModule,
    ReactiveFormsModule,
    LibroModule,
    SocioModule,
    PrestamoModule,
    AngularFireAuthModule,
    AngularFirestoreModule,

  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
