import { Injectable } from '@angular/core';
import {AngularFirestore} from "@angular/fire/compat/firestore";


@Injectable({
  providedIn: 'root'
})
export class PrestamoService {

  constructor(private angularfirestore: AngularFirestore) { }

  //Crear un prestamo nuevo

  public crearPrestamo(data:{socio:any, libro:any, fechaPrestamo:any, fechaDevolucion:any}){
    return this.angularfirestore.collection('Prestamo').add(data);
  }

  //obtiene un prestamo



  public obtenerPrestamo(documentID: string){
    return this.angularfirestore.collection('Prestamo').doc(documentID).snapshotChanges();
  }

  //obtener todos los prestamos
  public obtenerPrestamos(){
    return this.angularfirestore.collection('Prestamo').snapshotChanges();
  }
    //actualiza prestamo

    public actualizarPrestamo(documentId:string, data: any){
      return this.angularfirestore.collection('Prestamo').doc(documentId).set(data);
    }

    public deletePrestamo(documentId:string){
      return this.angularfirestore.collection('Prestamo').doc(documentId).delete();
    }

    public vaciarPrestamo(){
    id:'';
    libro:'';
    socio:'';
    fechaDevolucion:'';
    fechaPrestamo:'';
    }

  public obtenerPrestamoNoDevuelto(){
    return  this.angularfirestore.collection('Prestamo', ref =>ref.where ('fechaDevolucion',"==",null )).snapshotChanges();
  }
}
