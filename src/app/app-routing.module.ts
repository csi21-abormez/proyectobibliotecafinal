import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {SignInComponent} from "./Componentes/login/sign-in/sign-in.component";
import {SignUpComponent} from "./Componentes/login/sign-up/sign-up.component";
import {DashboardComponent} from "./Componentes/login/dashboard/dashboard.component";
import {AuthGuard} from "./Servicios/guardlogin/auth.guard";
import {ForgotPasswordComponent} from "./Componentes/login/forgot-password/forgot-password.component";
import {VerifyEmailComponent} from "./Componentes/login/verify-email/verify-email.component";


const routes: Routes = [

  {path: 'Libro', loadChildren :() => import('./Componentes/Libro/libro.module').then(m => m.LibroModule), canActivate: [AuthGuard]
},
  {
    path:'Prestamo', loadChildren : () => import('./Componentes/prestamo/prestamo.module').then(p => p.PrestamoModule) , canActivate: [AuthGuard]
  },

  {
    path:'Socio', loadChildren: () => import('./Componentes/socio/socio.module').then(s => s.SocioModule) ,canActivate: [AuthGuard]
  },
  { path: 'sign-in', component: SignInComponent },
  { path: 'register-user', component: SignUpComponent },
  { path: 'dashboard' , component: DashboardComponent, canActivate: [AuthGuard] },
  { path: 'forgot-password', component: ForgotPasswordComponent },
  { path: 'verify-email-address', component: VerifyEmailComponent },

  {
    path: '',
    redirectTo: 'sing-in',
    pathMatch: 'full'
  },
  {
    path:'**',component: SignInComponent
  }


];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
